package com.android.recyclerviewtask

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    var image: Int = R.drawable.ic_android,
    var firstName: String = "",
    var lastName: String = "",
    var email: String = "",
    var birthDate: String = "",
    var gender: String = ""
) : Parcelable
