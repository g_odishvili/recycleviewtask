package com.android.recyclerviewtask

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.android.recyclerviewtask.databinding.ActivityProfileInfoBinding

class ProfileInfo : AppCompatActivity() {
    private lateinit var binding: ActivityProfileInfoBinding
    private lateinit var user: User


    companion object {
        const val FIRST_NAME = "First Name: "
        const val LAST_NAME = "Last Name: "
        const val EMAIL = "Email: "
        const val GENDER = "Gender: "
        const val DATE_OF_BIRTH = "Date Of Birth: "
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    @SuppressLint("SetTextI18n")
    private fun init() {
        user = intent?.getParcelableExtra("clickedUser")!!
        intent.extras?.remove("clickedUser")
        binding.ivImage.setImageResource(user.image)
        binding.tvFirstName.text = FIRST_NAME + user.firstName
        binding.tvLastName.text = LAST_NAME + user.lastName
        binding.tvEmail.text = EMAIL + user.email
        binding.tvBirthDate.text = DATE_OF_BIRTH + user.birthDate
        binding.tvGender.text = GENDER + user.gender
    }

}