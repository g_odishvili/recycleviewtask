package com.android.recyclerviewtask

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.recyclerviewtask.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var adapter: RecyclerViewAdapter
    private lateinit var binding: ActivityMainBinding
    private val items = mutableListOf<User>()

    companion object {
        const val USER_REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setData()
        init()

        binding.btnAdd.setOnClickListener {
            addUser()
        }
    }



      private fun init(){
        adapter = RecyclerViewAdapter(items)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter
    }


    private fun setData() {
        items.add(User(R.drawable.ic_android,"Giorgi","Foo"))
        items.add(User(R.drawable.ic_launcher_background,"Nika","Bar"))
        items.add(User(R.drawable.ic_confetti_doodles,"Beso","Class"))
        items.add(User(R.drawable.ic_launcher_foreground,"Sandro","Nass"))
        items.add(User(R.drawable.ic_launcher_foreground,"Sandroasdasd","Nass"))
        items.add(User(R.drawable.ic_launcher_foreground,"Sandro","Nassasdasdasd"))
        items.add(User(R.drawable.ic_launcher_foreground,"Sandroasdasd","Nassasdasdasd"))
    }


    private fun addUser() {
        val intent = Intent(this, NewUser::class.java)
        intent.putExtra("user", User())
        startActivityForResult(intent, USER_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == USER_REQUEST_CODE && resultCode == RESULT_OK) {
            val user: User =  data?.getParcelableExtra("updatedUser")!!
            intent.extras?.remove("updatedUser")
            items.add(user)
            adapter.notifyItemInserted(items.size)
        }
    }
}