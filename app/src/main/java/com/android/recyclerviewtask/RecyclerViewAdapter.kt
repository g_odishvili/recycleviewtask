package com.android.recyclerviewtask

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.android.recyclerviewtask.databinding.UserLayoutBinding

class RecyclerViewAdapter(private val users: MutableList<User>) :
    RecyclerView.Adapter<RecyclerViewAdapter.UserViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(UserLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int = users.size

    inner class UserViewHolder(private val binding: UserLayoutBinding) : RecyclerView.ViewHolder(binding.root){
        private lateinit var userFromList: User
        @SuppressLint("SetTextI18n")
        fun bind(){
            userFromList = users[adapterPosition]
            binding.ivImage.setImageResource( userFromList.image)
            binding.tvName.text = userFromList.firstName + " "+ userFromList.lastName
            binding.root.setOnClickListener {
                val intent = Intent(
                    it.context,
                    ProfileInfo::class.java
                )
                intent.putExtra("clickedUser", users[adapterPosition])
                startActivity(it.context,intent,null);
            }
        }
    }
}